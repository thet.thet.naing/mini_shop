<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;


class SearchController extends Controller
{
    public function search(Request $request)
    {
        $request->validate([
            'search' => 'required',
        ]);
        $search=$request->search;
        $products = DB::table('products')->where('name', 'LIKE', "%$search%" )
                                         ->orWhere('description','LIKE',"%$search%")
                                         ->orWhere('price','LIKE',"%$search%")
                                         ->get();
        return view('backend.Product.index',compact('products','search'));
    
    }
}

<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function frontendfun(){
        $products = Product::all();
        return view('frontend.home',compact('products'));
    }
}

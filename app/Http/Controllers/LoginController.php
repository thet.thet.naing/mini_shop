<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function register(){
        return view('auth.registerform');
    }

    public function save(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'permissioncd' => 'required',
        ]);
        // Data insert

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->get('password')); 
        $user->permissioncd = $request->permissioncd;

        $user->save();
        return redirect()->route('login');
    }
    
    public function login(){
        return view('auth.login');
    }

    public function check(Request $request){
        $rules = array(
            'email'    => 'required', 
            'password' => 'required', 
            'permissioncd'=>'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            $userdata = array(
                'email'     => $request->get('email'),
                'password'  => $request->get('password'),
                'permissioncd'  => $request->get('permissioncd')
            );

            if (Auth::attempt($userdata)) {
                $roles = $request->permissioncd;
                switch($roles){
                    case "01":
                        return redirect('product');
                        break;
                    case "02":
                        return redirect('/');
                        break;
                }

                return redirect()->route('userlist');
            } else {        

             return back()->with('error', 'Your email,password and role are invalid!Please fill the correct email,password and role.');

            }

        }

    }

    public function userlist(){
        $userlists = User::all();
        return view('backend.user.userlist',compact('userlists'));
    }

    public function userdelete(Request $request,$id){
        $user = User::find($id);
        $user->delete();
        return redirect()->route('userlist');

    }

    public function useredit(Request $request,$id){
        $user = User::find($id);
        return view('backend.user.useredit',compact('user'));
    }

    public function userupdate(Request $request,$id){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'permissioncd' => 'required',
        ]);
        // Data insert
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->permissioncd = $request->permissioncd;

        $user->save();
        return redirect()->route('userlist');
  
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect()->route('login');
      }
      

}

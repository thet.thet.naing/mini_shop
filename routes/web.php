<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/','FrontendController@frontendfun')->name('homepage');
    Route::get('register','LoginController@register')->name('register');
    Route::get('login','LoginController@login')->name('login');
    Route::post('save','LoginController@save')->name('save');
    Route::post('check','LoginController@check')->name('check');
    Route::get('home','FrontendController@frontendfun')->name('homepage');
    Route::post('logout','LoginController@logout')->name('logout');

    
    Route::middleware('can:only_admin')->group(function () {

        Route::get('dashboard','BackendController@dashboardfun')->name('dashboardpage');
        Route::get('search','SearchController@search')->name('search');
        Route::resource('product','ProductController');
        Route::get('userlist','LoginController@userlist')->name('userlist');
        Route::delete('userdelete/{id}','LoginController@userdelete')->name('userdelete');
        Route::get('useredit/{id}','LoginController@useredit')->name('useredit');
        Route::put('useredit/{id}','LoginController@userupdate')->name('userupdate');
    });   


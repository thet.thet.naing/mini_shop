<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content=""/>
  <meta name="author" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title> Shopules </title>

  <!-- Favicon-->

  <!-- iconfont CSS -->
  <!-- Boxicon CSS -->
  
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/font.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/style.css')}}">
  
  <!-- BOOTSTRAP CSS -->
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/bootstrap.min.css')}}">

  <!-- OWL CAROUSEL -->
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/owl.carousel.css')}}">

</head>
<body>

  <!-- Navigation-->
      
      <!-- Search Bar -->
      <div class="col-xl-6 col-lg-6 col-md-4 col-sm-2 col-2 order-xl-2 order-lg-2 order-md-3 order-sm-3 order-3">
        <div class="row">
          <div class="col-lg-8 col-2 ">
          </div>
          <div class="col-lg-4 col-10">
            



            <span classs="float-right d-xl-block d-lg-block d-md-block d-none">
              <a href="{{route('login')}}" class="text-decoration-none loginLink"> Login </a> | <a href="{{route('register')}}" class="text-decoration-none loginLink"> Sign-up </a>
            </span>

             
                  
          </div>
        </div>
      </div>
       
     
  <!-- Sub Nav (WEB) -->
  <!-- <div class="container subNav d-xl-block d-lg-block d-none my-3">
    <div class="row align-items-center">
      <div class="col-3 align-items-center">
        <p class="d-inline pr-3"> Shop By </p>

        <div class="dropdown d-inline-block">
                <a class="nav-link text-decoration-none text-dark font-weight-bold d-block" href="javascript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="mr-2"> Product </span>
            <i class="icofont-rounded-down pt-2"></i>

              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              @foreach($products as $product)
                  <li class="dropdown-submenu">
                    <a class="dropdown-item" href="javascript:void(0)">
                      {{$product->name}}
                    </a>
                  </li>
                  <div class="dropdown-divider"></div>

                  @endforeach

              </ul>
            </div>
      </div>

      
    </div>
  </div> -->

  
  
  @yield('content')


  <!-- Footer -->
  
  
  <script type="text/javascript" src="{{ asset('frontend/js/jquery.min.js')}}"></script>
  <!-- BOOTSTRAP JS -->
  <script type="text/javascript" src="{{ asset('frontend/js/bootstrap.bundle.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <script type="text/javascript" src="{{ asset('frontend/js/custom.js')}}"></script>

  <!-- Owl Carousel -->
  <script type="text/javascript" src="{{ asset('frontend/js/owl.carousel.js')}}"></script>

</body>
</html>
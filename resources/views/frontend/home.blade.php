@extends('frontendtemplate')

@section('content')
  <!-- Carousel -->
  <!-- <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      
      <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('frontend/image/banner/ac.jpg')}}" class="d-block w-100 bannerImg" alt="...">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('frontend/image/banner/giordano.jpg')}}" class="d-block w-100 bannerImg" alt="...">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('frontend/image/banner/garnier.jpg')}}" class="d-block w-100 bannerImg" alt="...">
        </div>
      </div>
  </div> -->

  <!-- Content -->
<div class="container mt-5 px-5">
    
    <div class="row mt-5">
      <h1> All Product </h1>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                @foreach($products as $product)
                    <div class="item">
                        <div class="pad15">
                          <img src="{{ asset($product->photo)}}" class="img-fluid">
                            <p class="text-truncate">{{$product->name}}</p>
                            <p class="item-price">
                              <span class="d-block">{{$product->price}} Ks</span>
                            </p>

                            <a href="#" class="addtocartBtn text-decoration-none">Add to Cart</a>

                        </div>
                    </div>
                    @endforeach;
                    
                </div>
                <button class="btn btnMain leftLst"><</button>
                <button class="btn btnMain rightLst">></button>
            </div>
        </div>
    </div>

</div>
@endsection
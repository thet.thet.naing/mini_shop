@extends('backendtemplate')

@section('content')
	<div class="container-fluid">
    <!-- Page Heading -->
  	<div class="row">
  		<div class="col-md-12 mb-3">
    		<h1 class="h3 mb-0 text-gray-800 d-inline-block">Product Detail</h1>
  		</div>
  	</div>
    
    <div class="row">
    	<div class="col-md-6">
    		<img src="../{{$productdetail->photo}}" class="img-fluid img-thumbnail" >
    	</div>  
        <div class="col-md-6" style="margin-top:240px;">
    		<div class="text-center">{{$productdetail->name}}</div>
            <div class="text-center">{{$productdetail->description}}</div>
    		<div class="text-center">{{$productdetail->price}}</div>
    	</div>
    </div>

 	</div>
@endsection
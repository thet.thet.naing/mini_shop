@extends('backendtemplate')

@section('content')
	<div class="container-fluid">
    <!-- Page Heading -->
  	<div class="row">
  		<div class="col-md-12 mb-3">
    		<h1 class="h3 mb-0 text-gray-800 d-inline-block">Product List</h1>
  		</div>
  	</div>
    
    <div class="row">
    	<div class="col-md-12">
    		<table class="table table-bordered">
    			<thead class="thead-dark">
    				<tr>
                        <th>No</th>
    					<th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
    				</tr>
    			</thead>
    			<tbody>
                @php $i=1; @endphp
                    @foreach($userlists as $userlist)
    				<tr>
                        <td>{{$i++}}</td>    
                        <td>{{$userlist->name}}</td>            
                        <td>{{$userlist->email}}</td> 
                        @foreach(\App\User::PERMISSION_LIST as $key => $value )
                            @if(old('permissioncd', $userlist->permissioncd ?? null) === $key) <td> {{$value}} </td> @endif
                        @endforeach
           
                        <td>
                            <a href="{{route('useredit',$userlist->id)}}" class="btn btn-outline-warning"><i class="fas fa-edit"></i></a>

                            <form action="{{route('userdelete',$userlist->id)}}" method="POST" class="d-inline-block">
                                @csrf
                                @method('DELETE')
                                <!-- delete lote yin method ka post nat ma ya buu dar kout method ko delete so pe change pay ya mal update lo myoe pot -->
                                <button class="btn btn-outline-danger "><i class="fas fa-trash"></i></button>
                            </form>
                        </td>            

                    </tr>
                    @endforeach
    			</tbody>
    		</table>
    	</div>
    </div>

 	</div>
@endsection
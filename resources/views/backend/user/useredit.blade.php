@extends('backendtemplate')

@section('content')
	<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    	<div class="row">
    		<div class="col">
      		<h1 class="h3 mb-0 text-gray-800">User Edit Form</h1>
    		</div>
    	</div>
    </div>
    
    <div class="container">
      <div class="row">
      	<div class="col-md-12">
      		<form action="{{route('userupdate',$user->id)}}" method="post" enctype="multipart/form-data">
			      @csrf
			      @method('PUT')
			      
			       
			      <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
			        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
			        <div class="col-sm-5">
			          <input type="text" class="form-control" id="inputName" name="name" value="{{$user->name}}">
			          <span class="text-danger">{{ $errors->first('name') }}</span>
			        </div>
			      </div>

                  <div class="form-group row {{ $errors->has('email') ? 'has-error' : '' }}">
			        <label for="inputName" class="col-sm-2 col-form-label">Email</label>
			        <div class="col-sm-5">
			          <input type="text" class="form-control" id="inputName" name="email" value="{{$user->email}}">
			          <span class="text-danger">{{ $errors->first('email') }}</span>
			        </div>
			      </div>

                  <div class="form-group row">
                    <label for="permissioncd" class="col-sm-2 col-form-label ">User Role</label>

                    <div class="col-sm-5">
                        <select id="permissioncd"
                                class="form-control @error('permissioncd') is-invalid @enderror"
                                name="permissioncd">
                            @foreach(\App\User::PERMISSION_LIST as $key => $value )
                                <option value="{{ $key }}"
                                        @if(old('permissioncd', $user->permissioncd ?? null) === $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>

                        @error('permissioncd')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>


			      <div class="form-group row">
			        <div class="col-sm-5">
			          <input type="submit" class="btn btn-primary" name="btnsubmit" value="Update">
			        </div>
			      </div>
			    </form>
      	</div>
      </div>
    </div>
 	</div>
@endsection